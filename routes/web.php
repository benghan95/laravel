<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('dashboard');
});

Route::middleware(['secure'])->group(function() {
    Auth::routes();
});


Route::middleware(['nonsecure'])->group(function () {
    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/dashboard', 'DashboardController@index')->name('dashboard');

    Route::get('/list-banners', 'BannerController@index')->name('list-banners');
    Route::get('/create-banner', 'BannerController@create')->name('create-banner');
    Route::get('/edit-banner/{hash}', 'BannerController@edit')->name('edit-banner');
    Route::get('/export-banner/{hash}', 'BannerController@export')->name('export-banner');
    Route::get('/export-banner/{hash}/{type}', 'BannerController@exportWithType')->name('export-banner-with-type');


    Route::get('/list-bannersets', 'BannersetController@index')->name('list-bannersets');
    Route::get('/create-bannerset', 'BannersetController@create')->name('create-bannerset');
    Route::get('/edit-bannerset/{hash}', 'BannersetController@edit')->name('edit-bannerset');
});
