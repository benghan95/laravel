<?php

namespace App\Http\Controllers;

use Log;
use Auth;
use DateTime;
use Illuminate\Http\Request;

class BannerController extends Controller
{
    private $api;
    private $client_id;
    private $secret_key;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

        $this->api        = config('api.bannersnack.api');
        $this->client_id  = config('api.bannersnack.client_id');
        $this->secret_key = config('api.bannersnack.secret_key');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();

        if (!$user)
            return redirect()->route('login');

        $action     = 'get-user-banners';
        $now        = new DateTime();
        $timestamp  = $now->getTimestamp();
        $identifier = $user->email;

        $concat    = 'action' . $action . 'client_id' . $this->client_id . 'identifier' . $identifier . 't' . $timestamp . 'typeall';
        $signature = hash_hmac('sha1', $concat, $this->secret_key);

        $url = $this->api . '?action=' . $action . '&t=' . $timestamp . '&identifier=' . $identifier . '&client_id=' . $this->client_id . '&signature=' . $signature . '&type=all';

        $response = file_get_contents($url);
        $json = json_decode($response, true);

        $data = [];
        $banner_list = collect();
        if ($json['code'] == 200) {
            foreach ($json['data'] as $item) {
                if ($item['type'] === 'bannerset') {
                    $action     = 'get-bannerset-urls';
                    $now        = new DateTime();
                    $timestamp  = $now->getTimestamp();
                    $identifier = $user->email;

                    $concat    = 'action' . $action . 'bannerset_hash' . $item['bannerHash']  . 'client_id' . $this->client_id . 'identifier' . $identifier . 't' . $timestamp;
                    $signature = hash_hmac('sha1', $concat, $this->secret_key);

                    $url = $this->api . '?action=' . $action . '&t=' . $timestamp . '&identifier=' . $identifier . '&client_id=' . $this->client_id . '&signature=' . $signature . '&bannerset_hash=' . $item['bannerHash'];

                    $resp = file_get_contents($url);
                    $item_json = json_decode($resp, true);

                    if ($item_json['code'] === 200) {
                        $item['banner_items'] = $item_json['data'];
                    }
                }
                $banner_list->push($item);
            }
        }

        $data['banners'] = $banner_list;

        return view('banner.index', $data);
    }

    public function create()
    {
        $user = Auth::user();

        if (!$user)
            return redirect()->route('login');

        $action     = 'create-banner';
        $now        = new DateTime();
        $timestamp  = $now->getTimestamp();
        $identifier = $user->email;

        $concat    = 'action' . $action . 'client_id' . $this->client_id . 'identifier' . $identifier . 't' . $timestamp;
        $signature = hash_hmac('sha1', $concat, $this->secret_key);

        $url = $this->api . '?action=' . $action . '&t=' . $timestamp . '&identifier=' . $identifier . '&client_id=' . $this->client_id . '&signature=' . $signature;

        $response = file_get_contents($url);
        $json = json_decode($response, true);

        $data = [];
        if ($json['code'] == 200) {
            $data['iframe_url'] = 'http://' . $json['data']['url'];
        } else {
            // Return error
        }

        return view('banner.create', $data);
    }

    public function edit($hash)
    {
        $user = Auth::user();

        if (!$user)
            return redirect()->route('login');

        $action     = 'edit-banner';
        $now        = new DateTime();
        $timestamp  = $now->getTimestamp();
        $identifier = $user->email;

        $concat    = 'action' . $action . 'banner_hash' . $hash . 'client_id' . $this->client_id . 'identifier' . $identifier . 't' . $timestamp;
        $signature = hash_hmac('sha1', $concat, $this->secret_key);

        $url = $this->api . '?action=' . $action . '&t=' . $timestamp . '&identifier=' . $identifier . '&client_id=' . $this->client_id . '&signature=' . $signature . '&banner_hash=' . $hash;

        $response = file_get_contents($url);
        $json = json_decode($response, true);

        $data = [];
        if ($json['code'] == 200) {
            $data['iframe_url'] = 'http://' . $json['data']['url'];
        } else {
            // Return error
        }

        return view('banner.create', $data);
    }

    public function export($hash)
    {
        $user = Auth::user();

        if (!$user)
            return redirect()->route('login');

        $action     = 'get-banner-urls';
        $now        = new DateTime();
        $timestamp  = $now->getTimestamp();
        $identifier = $user->email;

        $concat    = 'action' . $action . 'banner_hash' . $hash . 'client_id' . $this->client_id . 'identifier' . $identifier . 't' . $timestamp;
        $signature = hash_hmac('sha1', $concat, $this->secret_key);

        $url = $this->api . '?action=' . $action . '&t=' . $timestamp . '&identifier=' . $identifier . '&client_id=' . $this->client_id . '&signature=' . $signature . '&banner_hash=' . $hash;

        $response = file_get_contents($url);
        $json = json_decode($response, true);

        $data = [];
        if ($json['code'] == 200) {
            $data['exports'] = $json['data'];
            $data['hash']    = $hash;
        } else {
            // Return error
        }

        return view('banner.export', $data);
    }

    public function exportWithType($hash, $type)
    {
        $user = Auth::user();

        if (!$user)
            return redirect()->route('login');

        $action     = 'start-banner-export';
        $now        = new DateTime();
        $timestamp  = $now->getTimestamp();
        $identifier = $user->email;

        $concat    = 'action' . $action . 'banner_hash' . $hash . 'client_id' . $this->client_id . 'identifier' . $identifier . 't' . $timestamp . 'type' . $type;

        Log::debug($concat);
        $signature = hash_hmac('sha1', $concat, $this->secret_key);

        $url = $this->api . '?action=' . $action . '&t=' . $timestamp . '&banner_hash=' . $hash . '&type=' . $type . '&identifier=' . $identifier . '&client_id=' . $this->client_id . '&signature=' . $signature;
        Log::debug($url);

        $response = file_get_contents($url);
        $json     = json_decode($response, true);
        Log::debug(serialize($json));

        $data = [];
        if ($json['code'] == 200) {
            $data['exports'] = $json['data'];
        } else {
            // Return error
        }

        return $json;
    }
}
