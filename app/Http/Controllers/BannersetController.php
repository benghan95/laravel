<?php

namespace App\Http\Controllers;

use Auth;
use DateTime;
use Illuminate\Http\Request;

class BannersetController extends Controller
{
    private $api;
    private $client_id;
    private $secret_key;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

        $this->api        = config('api.bannersnack.api');
        $this->client_id  = config('api.bannersnack.client_id');
        $this->secret_key = config('api.bannersnack.secret_key');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();

        if (!$user)
            return redirect()->route('login');


        return view('bannerset.index', $data);
    }

    public function create()
    {
        $user = Auth::user();

        if (!$user)
            return redirect()->route('login');

        $action     = 'create-bannerset';
        $now        = new DateTime();
        $timestamp  = $now->getTimestamp();
        $identifier = $user->email;

        $concat    = 'action' . $action . 'client_id' . $this->client_id . 'identifier' . $identifier . 't' . $timestamp;
        $signature = hash_hmac('sha1', $concat, $this->secret_key);

        $url = $this->api . '?action=' . $action . '&t=' . $timestamp . '&identifier=' . $identifier . '&client_id=' . $this->client_id . '&signature=' . $signature;

        $response = file_get_contents($url);
        $json = json_decode($response, true);

        $data = [];
        if ($json['code'] == 200) {
            $data['iframe_url'] = 'http://' . $json['data']['url'];
        } else {
            // Return error
        }

        return view('bannerset.create', $data);
    }

    public function edit($hash)
    {
        $user = Auth::user();

        if (!$user)
            return redirect()->route('login');

        $action     = 'edit-bannerset';
        $now        = new DateTime();
        $timestamp  = $now->getTimestamp();
        $identifier = $user->email;

        $concat    = 'action' . $action . 'bannerset_hash' . $hash . 'client_id' . $this->client_id . 'identifier' . $identifier . 't' . $timestamp;
        $signature = hash_hmac('sha1', $concat, $this->secret_key);

        $url = $this->api . '?action=' . $action . '&t=' . $timestamp . '&identifier=' . $identifier . '&client_id=' . $this->client_id . '&signature=' . $signature . '&bannerset_hash=' . $hash;

        $response = file_get_contents($url);
        $json = json_decode($response, true);

        $data = [];
        if ($json['code'] == 200) {
            $data['iframe_url'] = 'http://' . $json['data']['url'];
        } else {
            // Return error
        }

        return view('banner.create', $data);
    }

    public function export($hash)
    {
        $user = Auth::user();

        if (!$user)
            return redirect()->route('login');

        $action     = 'get-bannerset-urls';
        $now        = new DateTime();
        $timestamp  = $now->getTimestamp();
        $identifier = $user->email;

        $concat    = 'action' . $action . 'banner_set' . $hash . 'client_id' . $this->client_id . 'identifier' . $identifier . 't' . $timestamp;
        $signature = hash_hmac('sha1', $concat, $this->secret_key);

        $url = $this->api . '?action=' . $action . '&t=' . $timestamp . '&identifier=' . $identifier . '&client_id=' . $this->client_id . '&signature=' . $signature . '&banner_set=' . $hash;

        $response = file_get_contents($url);
        $json = json_decode($response, true);

        $data = [];
        if ($json['code'] == 200) {
            $data['banners'] = $json['data'];
        } else {
            // Return error
        }

        return view('banner.export', $data);
    }
}
