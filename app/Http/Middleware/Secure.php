<?php

namespace App\Http\Middleware;

use App;
use Closure;

class Secure
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(App::environment() === 'production' && !$request->secure()) {
            return redirect()->secure(str_replace('bannercreator/', '', $request->getRequestUri()));
        }

        return $next($request);
    }
}
