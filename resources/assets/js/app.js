window.$ = window.jQuery = require('jquery');

$(document).ready(() => {
  $('.btn-export').each(function() {
    $(this).on('click', function (e) {
      if ($(this).attr('data-status') !== 'done') {
        e.preventDefault();

        const hash = $('#banner-hash').val();
        const type = $(this).attr('data-type');
        $(this).attr('data-status', 'pending');
        $.get(`/export-banner/${hash}/${type}`, (data) => {
          if (data.code == 200) {
            if (data.data.status == 'done') {
              checkStatus(hash, type);
            } else {
              alert("Your file is still being rendering, please refresh the page in 5 seconds");
            }
          } else {
            alert("Error occured! Please refresh the page and try again later.");
          }
        });
      }
    })
  })

  $('.btn-export[data-status="pending"]').each(function() {
    checkStatus($('#banner-hash').val(), $(this).attr('data-type'));
  })
  // $('.btn-export[data-status="pending"]')[0].click();

  function getStatus(hash, type, callback) {
    $.get(`/export-banner/${hash}`, (res) => {
      const response = $(res);
      const button = response.find(`.btn-export[data-type="${type}"]`);
      console.log(button.attr('data-status'));
      callback(button.attr('data-status'));
    });
  }

  function checkStatus(hash, type) {
    setTimeout(() => {
      getStatus(hash, type, (res) => {
        if (res !== 'done') {
          checkStatus(hash, type);
        } else {
          $('.btn-export[data-type="' + type + '"]').attr('data-status', 'done');
        }
      });
    }, 2000);
  }
});