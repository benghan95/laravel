@extends('layouts.master')
@section('title', 'All Banners')
@section('page', 'list-banners')

@section('styles')
  <link rel="stylesheet" href="{{ asset('vendor/footable/css/footable.core.min.css') }}">
@endsection

@section('content')
  <section id="banner-list">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h2 class="mb-4">Manage Banners</h2>
          <div class="table-responsive mb-5">
            <table class="table mb-4 table-bordered">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Name</th>
                  <th>Preview</th>
                  <th>Date Created</th>
                  <th>Date Modified</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                @php ($i = 1)
                @foreach ($banners as $banner)
                  @if ($banner['type'] === 'banner')
                    <tr>
                      <td>{{ $i }}</td>
                      <td>
                        {{ $banner['name'] }}<br>
                        w: {{ $banner['width'] }}px x h: {{ $banner['height'] }}px
                      </td>
                      <td>
                        <img class="banner-preview" src="{{ $banner['image'] }}" alt="{{ $banner['name'] }}">
                      </td>
                      <td>{{ Carbon\Carbon::createFromTimestamp($banner['dateCreated'])->setTimezone('Asia/Kuala_Lumpur')->format('d.m.Y h:i A') }}</td>
                      <td>{{ Carbon\Carbon::createFromTimestamp($banner['dateLastUpdate'])->setTimezone('Asia/Kuala_Lumpur')->format('d.m.Y h:i A') }}</td>
                      <td>
                        <a class="btn btn-primary" href="{{ route('edit-banner', ['hash' => $banner['bannerHash']]) }}">Edit</a>
                        <a class="btn btn-success" href="{{ route('export-banner', ['hash' => $banner['bannerHash']]) }}">Export</a>
                      </td>
                    </tr>
                    @php ($i ++)
                  @endif
                @endforeach
              </tbody>
            </table>
          </div>

          <h2 class="mb-4">Manage Bannersets</h2>
          <div class="table-responsive">
            <table id="bannersets-list" class="table table-bordered">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Name</th>
                  <th>Preview</th>
                  <th>No. Of Banners</th>
                  <th>Date Created</th>
                  <th>Date Modified</th>
                  <th>Action</th>
                  <th data-hide="all">Banners</th>
                </tr>
              </thead>
              <tbody>
                @php ($i = 1)
                @foreach ($banners as $banner)
                  @if ($banner['type'] === 'bannerset')
                    <tr>
                      <td>{{ $i }}</td>
                      <td>
                        {{ $banner['name'] }}
                      </td>
                      <td>
                        <img class="banner-preview" src="{{ $banner['image'] }}" alt="{{ $banner['name'] }}">
                      </td>
                      <td>{{ $banner['numberOfBanners'] }}</td>
                      <td>{{ Carbon\Carbon::createFromTimestamp($banner['dateCreated'])->setTimezone('Asia/Kuala_Lumpur')->format('d.m.Y h:i A') }}</td>
                      <td>{{ Carbon\Carbon::createFromTimestamp($banner['dateLastUpdate'])->setTimezone('Asia/Kuala_Lumpur')->format('d.m.Y h:i A') }}</td>
                      <td>
                        <a class="btn btn-primary" href="{{ route('edit-bannerset', ['hash' => $banner['bannerHash']]) }}">Edit</a>
                      </td>
                      <td>
                        <table class="table table-bordered table-footable">
                          <thead>
                            <tr>
                              <th>#</th>
                              <th>Preview</th>
                              <th>Action</th>
                            </tr>
                          </thead>
                          <tbody>
                            @php ($j = 1)
                            @foreach ($banner['banner_items'] as $item)
                              <tr>
                              <td>{{ $j }}</td>
                              <td>
                                <img class="banner-preview" src="{{ $item['urls'][0]['url'] }}" alt="">
                              </td>
                              <td>
                                <a class="btn btn-primary" href="{{ route('edit-banner', ['hash' => $item['bannerHash']]) }}">Edit</a>
                                <a class="btn btn-success" href="{{ route('export-banner', ['hash' => $item['bannerHash']]) }}">Export</a>
                              </td>
                              </tr>
                              @php ($j ++)
                            @endforeach
                          </tbody>
                        </table>
                      </td>
                    </tr>
                    @php ($i ++)
                  @endif
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </section>
@endsection

@section('scripts')
  <script src="{{ asset('vendor/footable/js/footable.js') }}"></script>
  <script src="{{ asset('js/pages/list-banners.js') }}"></script>
@endsection
