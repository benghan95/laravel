@extends('layouts.master')
@section('title', 'Create Banner')
@section('page', 'create-banner')

@section('content')
  <div class="iframe-wrapper">
    <iframe src="{{ $iframe_url }}" frameborder="0"></iframe>
  </div>
@endsection
