@extends('layouts.master')
@section('title', 'Edit Banners')
@section('page', 'edit-banner')

@section('content')
  <div class="iframe-wrapper">
    <iframe src="{{ $iframe_url }}" frameborder="0"></iframe>
  </div>
@endsection
