@extends('layouts.master')
@section('title', 'Export Banner')
@section('page', 'export-banners')

@section('content')
  <section id="export-actions">
    <div class="centering">
      <div class="container">
        <div class="row">
          <div class="col-md-12">

            <h2 class="mb-4">Export</h2>

            <div class="buttons mb-4">

              @foreach ($exports as $export)
                @if ($export['type'] === 'jpg')
                  <a class="btn btn-export btn-outline-primary" href="{{ $export['url'] }}" download="{{ $hash }}.jpg" data-status="{{ $export['status'] }}" data-type="{{ $export['type'] }}" target="_blank">
                    JPG File
                    <i class="far fa-times"></i>
                    <i class="far fa-check"></i>
                    <img src="{{ asset('images/loading.gif') }}" class="image-loading" alt="">
                  </a>
                @endif
                @if ($export['type'] === 'png')
                  <a class="btn btn-export btn-outline-primary" href="{{ $export['url'] }}" download="{{ $hash }}.png" data-status="{{ $export['status'] }}" data-type="{{ $export['type'] }}" target="_blank">
                    PNG File
                    <i class="far fa-times"></i>
                    <i class="far fa-check"></i>
                    <img src="{{ asset('images/loading.gif') }}" class="image-loading" alt="">
                  </a>
                @endif
                @if ($export['type'] === 'html5')
                  <a class="btn btn-export btn-outline-primary" href="{{ $export['url'] }}" download="{{ $hash }}.zip" data-status="{{ $export['status'] }}" data-type="{{ $export['type'] }}" target="_blank">
                    HTML5 Zip File
                    <i class="far fa-times"></i>
                    <i class="far fa-check"></i>
                    <img src="{{ asset('images/loading.gif') }}" class="image-loading" alt="">
                  </a>
                @endif
                @if ($export['type'] === 'mp4')
                  <a class="btn btn-export btn-outline-primary" href="{{ $export['url'] }}" data-status="{{ $export['status'] }}" data-type="{{ $export['type'] }}" target="_blank" download="{{ $hash }}.mp4">
                    MP4 File
                    <i class="far fa-times"></i>
                    <i class="far fa-check"></i>
                    <img src="{{ asset('images/loading.gif') }}" class="image-loading" alt="">
                  </a>
                @endif
                @if ($export['type'] === 'animated-gif')
                  <a class="btn btn-export btn-outline-primary" href="{{ $export['url'] }}" data-status="{{ $export['status'] }}" data-type="{{ $export['type'] }}" target="_blank" download="{{ $hash }}.gif">
                    GIF File
                    <i class="far fa-times"></i>
                    <i class="far fa-check"></i>
                    <img src="{{ asset('images/loading.gif') }}" class="image-loading" alt="">
                  </a>
                @endif
                @if ($export['type'] === 'optimized-animated-gif')
                  <a class="btn btn-export btn-outline-primary" href="{{ $export['url'] }}" data-status="{{ $export['status'] }}" data-type="{{ $export['type'] }}" target="_blank" download="{{ $hash }}.gif">
                    Optimized GIF File
                    <i class="far fa-times"></i>
                    <i class="far fa-check"></i>
                    <img src="{{ asset('images/loading.gif') }}" class="image-loading" alt="">
                  </a>
                @endif
              @endforeach

              @csrf
              <input type="hidden" id="banner-hash" name="banner_hash" value="{{ $hash }}">

            </div>

            <p><strong>Click MP4, GIF and Optimized GIF to start rendering.</strong><br>
              After few minutes, your file will be ready to download.</p>

          </div>
        </div>
      </div>
    </div>
  </section>
@endsection
