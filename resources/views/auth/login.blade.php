@extends('layouts.master')
@section('title', 'Login')
@section('page', 'login')

@section('content')
  <form class="form-signin" action="{{ route('login') }}" aria-label="{{ __('Login') }}" method="POST">
    @csrf

    <img class="mb-4 img-fluid" src="{{ asset('images/naga-logo-black.png') }}" alt="">
    <!-- <h1 class="h3 mb-3 font-weight-normal">Login</h1> -->

    <div class="form-group">
      <label for="email" class="sr-only">Email address</label>
      <input type="email" id="email" name="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="Email address" required autofocus value="{{ old('email') }}">

      @if ($errors->has('email'))
        <span class="invalid-feedback" role="alert">
          <strong>{{ $errors->first('email') }}</strong>
        </span>
      @endif
    </div>

    <div class="form-group">
      <label for="password" class="sr-only">Password</label>
      <input type="password" id="password" name="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" placeholder="Password" required>

      @if ($errors->has('password'))
        <span class="invalid-feedback" role="alert">
          <strong>{{ $errors->first('password') }}</strong>
        </span>
      @endif
    </div>

    <div class="form-group mb-3">
      <div class="checkbox mb-3">
        <label>
          <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember me
        </label>
      </div>
    </div>

    <div class="form-group">

      <button class="btn btn-lg btn-primary btn-block mb-2" type="submit">Sign in</button>

      <a class="btn-link" href="{{ route('password.request') }}">
          {{ __('Forgot Your Password?') }}
      </a>

    </div>
  </form>
@endsection
