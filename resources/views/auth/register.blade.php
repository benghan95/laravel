@extends('layouts.master')
@section('title', 'Register')
@section('page', 'login')

@section('content')
<form class="form-signin" method="POST" action="{{ route('register') }}" aria-label="{{ __('Register') }}">
    @csrf

    <img class="mb-4" src="{{ asset('images/naga-logo-black.png') }}" alt="" width="auto" height="72">
    <h4 class="text-center">Yay!</h4>
    <p class="text-center mb-4">Congratulation! This is your special invitation to be a Nagabanner member.</p>

    <div class="form-group row">
        <label for="name" class="sr-only">{{ __('Name') }}</label>
        <input id="name" placeholder="{{ __('Name') }}" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>

        @if ($errors->has('name'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
        @endif
    </div>

    <div class="form-group row">
        <label for="email" class="sr-only">{{ __('E-Mail Address') }}</label>
        <input id="email" placeholder="{{ __('E-Mail Address') }}" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

        @if ($errors->has('email'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
        @endif
    </div>

    <div class="form-group row">
        <label for="password" class="sr-only">{{ __('Password') }}</label>
        <input id="password" placeholder="{{ __('Password') }}" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

        @if ($errors->has('password'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('password') }}</strong>
            </span>
        @endif
    </div>

    <div class="form-group row mb-4">
        <label for="password-confirm" class="sr-only">{{ __('Confirm Password') }}</label>
        <input id="password-confirm" placeholder="{{ __('Confirm Password') }}" type="password" class="form-control" name="password_confirmation" required>
    </div>

    <div class="form-group row mb-0">
        <button type="submit" class="btn btn-lg btn-primary btn-block">
            {{ __('Register') }}
        </button>
    </div>
</form>
                    
@endsection
