@extends('layouts.master')
@section('title', 'PasswordReset')
@section('page', 'login')

@section('content')
<form class="form-signin" method="POST" action="{{ route('password.email') }}" aria-label="{{ __('Reset Password') }}">
    @csrf

    <img class="mb-4" src="{{ asset('images/naga-logo-black.png') }}" alt="" width="auto" height="72">
    <h4 class="text-center mb-4">{{ __('Reset Password') }}</h4>

    @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session('status') }}
        </div>
    @endif

    <div class="form-group row">
        <label for="email" class="sr-only">{{ __('E-Mail Address') }}</label>
        <input id="email" placeholder="{{ __('E-Mail Address') }}" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }} mb-2" name="email" value="{{ old('email') }}" required>

        @if ($errors->has('email'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
        @endif
    </div>

    <div class="form-group row mb-0">
        <button type="submit" class="btn btn-lg btn-primary btn-block">
            {{ __('Email Password Reset Link') }}
        </button>
    </div>
</form>
@endsection
