<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Remedy for your banner needs.">
    {{-- <meta name="description" content="@yield('meta_description')"> --}}

    <title>@yield('title') - Bannerlogist</title>

    <!-- Favicon -->
    <link rel="icon" href="../../../../favicon.ico">

    <!-- Main Custom Style -->
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">

    @yield('styles')

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-111109295-2"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-111109295-2');
    </script>

  </head>
  <body id="page-@yield('page')">
    <!-- Header -->
    <header id="header">
      @component('partials.header')
      @endcomponent
    </header>

    <!-- Page content -->
    <div id="content">
      <main class="main">
        @yield('content')
      </main>
    </div>

    <!-- Footer -->
    <footer id="footer">
      @component('partials.footer')
      @endcomponent
    </footer>

    <!-- Main Script -->
    <script src="{{ asset('js/app.js') }}"></script>

    @yield('scripts')

  </body>
</html>