
@if (Auth::check())
  <div class="d-flex flex-column flex-md-row align-items-center">
    <a href="{{ route('dashboard') }}"><img class="naga-logo" src="{{ asset('images/naga-logo.png') }}" alt="" width="auto" height="56"></a>
    <nav class="my-2 my-md-0 mr-md-3">
    <a class="" href="{{ route('create-banner') }}">Create Banner</a>
      <a class="" href="{{ route('create-bannerset') }}">Create Bannerset</a>
      <a class="" href="{{ route('list-banners') }}">Manage Banners</a>
      <form class="d-inline-block btn-logout" action="{{ route('logout') }}" method="POST">
        @csrf
        <button type="submit" class="btn btn-logout">Logout</button>
      </form>
    </nav>
    {{-- <a class="btn btn-outline-primary btn-logout" href="#">Sign up</a> --}}
  </div>
@else
  <div class="d-flex flex-column flex-md-row align-items-center">
    <a href="/"><img class="naga-logo" src="{{ asset('images/naga-logo.png') }}" alt="" width="auto" height="56"></a>
    <nav class="my-2 my-md-0 mr-md-3">
      <a href="{{ route('login') }}">Login</a>
    </nav>
    <a class="btn btn-outline-primary" href="{{ route('register') }}">Sign up</a>
  </div>
@endif