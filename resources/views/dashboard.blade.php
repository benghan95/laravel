@extends('layouts.master')
@section('title', 'Dashboard')
@section('page', 'dashboard')

@section('content')
<section id="dashboard">
  <div class="container">
    <div class="row">
      <div class="col-md-12 mb-5">
        <h1>Welcome</h1>
      </div>

      <div class="col-md-4">
          <a href="{{ route('create-banner') }}">
            <figure>
              <img src="{{ asset('images/Time_Banner.svg') }}">
            </figure>
            <h3>Create a single banner</h3>
            <p>Yupe, you know how this works.</p>
          </a>
        </div>

        <div class="col-md-4">
            <a href="{{ route('create-bannerset') }}">
              <figure>
                <img src="{{ asset('images/Time_BannerSets.svg') }}">
              </figure>
              <h3>Create a banner set</h3>
              <p>Create a single design with multiple sizes at one go.</p>
        </div>

        <div class="col-md-4">
          <a href="{{ route('list-banners') }}">
            <figure>
              <img src="{{ asset('images/Manage.svg') }}">
            </figure>
            <h3>Manage banners</h3>
            <p>View created banners, edit or export them.</p>
          </a>
        </div>
    </div>
  </div>
</section>
@endsection
