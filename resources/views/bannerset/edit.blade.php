@extends('layouts.master')
@section('title', 'Edit Bannerset')
@section('page', 'edit-bannerset')

@section('content')
  <div class="iframe-wrapper">
    <iframe src="{{ $iframe_url }}" frameborder="0"></iframe>
  </div>
@endsection
