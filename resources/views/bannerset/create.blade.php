@extends('layouts.master')
@section('title', 'All Banners')
@section('page', 'list-banners')

@section('content')
  <div class="iframe-wrapper">
    <iframe src="{{ $iframe_url }}" frameborder="0"></iframe>
  </div>
@endsection
